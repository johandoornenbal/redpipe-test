package org.incode.redpipetest;

import net.redpipe.engine.core.Server;

public class App
{
    public static void main( String[] args )
    {
        new Server()
                .start(HelloResource.class)
                .subscribe(
                        () -> System.err.println("Server started"),
                        Throwable::printStackTrace);
    }
}
