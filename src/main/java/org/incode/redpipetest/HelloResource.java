package org.incode.redpipetest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("/")
public class HelloResource {

    @GET
    public String hello() {
        return "Hello World";
    }

    @POST
    public String postReply() {
        return "You have been posting";
    }
}
