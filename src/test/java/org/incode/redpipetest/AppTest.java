package org.incode.redpipetest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.reactivex.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import net.redpipe.engine.core.Server;

@RunWith(VertxUnitRunner.class)
public class AppTest {

    Server server;
    WebClient client;

    @Before
    public void setUp(TestContext tc) {
        Async async = tc.async();
        server = new Server();
        server.start(HelloResource.class)
            .subscribe(
            () -> {
                System.err.println("Server started");
                client = WebClient.create(server.getVertx(),
                        new WebClientOptions().setDefaultHost("localhost").setDefaultPort(9000));
                async.complete();
            },
            Throwable::printStackTrace);
    }

    @After
    public void close(TestContext tc){
        Async async = tc.async();
        server.close().subscribe(()->async.complete(), Throwable::printStackTrace);
    }

    @Test
    public void testGet(TestContext tc) {
        Async async = tc.async();
        client.get("/").rxSend().doOnError(
            rs ->tc.fail())
            .subscribe(
                ar -> {
                    tc.assertEquals(ar.statusCode(), 200);
                    tc.assertEquals(ar.bodyAsString(), "Hello World");
                    async.complete();
                }
            );
    }

    @Test
    public void testPost(TestContext tc) {
        Async async = tc.async();
        client.post("/").rxSend().doOnError(
                rs ->tc.fail())
                .subscribe(
                        ar -> {
                            tc.assertEquals(ar.statusCode(), 200);
                            tc.assertEquals(ar.bodyAsString(), "You have been posting");
                            async.complete();
                        }
                );
    }

    @Test
    public void testSwagger(TestContext tc) {
        Async async = tc.async();
        // swagger api is generated
        client.get("/swagger.json").rxSend().doOnError(
                rs ->tc.fail())
                .subscribe(
                        ar -> {
                            tc.assertEquals(ar.statusCode(), 200);
                            tc.assertFalse(ar.bodyAsJsonObject().isEmpty());
                            tc.assertTrue(ar.bodyAsJsonObject().containsKey("swagger"));
                            async.complete();
                        }
                );
    }

}
